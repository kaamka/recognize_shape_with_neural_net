# -*- coding: utf-8 -*-
"""
Created on Sun Jun 17 12:38:17 2018

@author: Kama
"""




#import tensorflow as tf
from matplotlib import pyplot as plt
from sklearn.model_selection import train_test_split
import numpy as np


#load datasets
T = np.load('triangles.npy')
C = np.load('circles.npy')
S = np.load('squares.npy')
H = np.load('hexagones.npy')

triangles = T.reshape((T.shape[0], 28, 28))
circles = C.reshape((C.shape[0], 28, 28))
squares = S.reshape((S.shape[0], 28, 28))
hexagons = H.reshape((H.shape[0], 28, 28))

basic_shapes = [triangles, circles,squares, hexagons]
number_of_samples = [triangles.shape[0],circles.shape[0],squares.shape[0],hexagons.shape[0]]
number_of_samples_fibb = [triangles.shape[0],triangles.shape[0]+circles.shape[0],triangles.shape[0]+circles.shape[0]+squares.shape[0],sum(number_of_samples)]
class_labels = ['triangles', 'circles','squares','hexagons']



#show few samples of data
def show_dataset_samples():
    global basic_shapes
    global number_of_samples
    for j,drawings in enumerate(basic_shapes):
        print("Total number of samples:", number_of_samples[j])
        #sample_image = drawings[555] # X[0].reshape((28, 28))
        for i,image in enumerate(drawings[:36]):
            plt.subplot(6,6,i+1)
            plt.imshow(image,cmap='Greys',  interpolation='nearest')
        plt.show()


show_dataset_samples()

# creates input and output matrix
X = np.concatenate((triangles, circles,squares,hexagons), axis=0)
Y = np.chararray(X.shape[0], itemsize=10)
Y[:number_of_samples_fibb[0]] = "triangle"
Y[number_of_samples_fibb[0]:number_of_samples_fibb[1]] = "circle"
Y[number_of_samples_fibb[1]:number_of_samples_fibb[2]] = "square"
Y[number_of_samples_fibb[2]:number_of_samples_fibb[3]] = "hexagon"

#split data
X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.33, random_state=42)
